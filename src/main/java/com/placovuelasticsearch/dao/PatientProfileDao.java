package com.placovuelasticsearch.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.placovuelasticsearch.databasemanager.DatabaseConnection;
import com.placovuelasticsearch.model.PatientProfile;
import com.placovuelasticsearch.model.PatientProfileMapper;
import com.placovuelasticsearch.spring.Employee;
import com.placovuelasticsearch.spring.EmployeeMapper;

@Repository
public class PatientProfileDao {
	@Autowired DatabaseConnection databaseConnection;
	
	private JdbcTemplate jdbcTemplate;
    
    public void setJdbcTemplate(JdbcTemplate template) {  
        this.jdbcTemplate = template;  
    } 
    
	public List<PatientProfile> getPatientProfileList()
	{
		//ArrayList<PatientProfile> profileList = new ArrayList<PatientProfile>();
		String sql = "select *from PatientProfile";
		List<PatientProfile> profileList = jdbcTemplate.query(sql, 
                        new PatientProfileMapper());
		/*Connection connection = databaseConnection.getConnection();
		if(connection == null)
			return profileList;
		
		String sql = "SELECT * FROM PatientProfile";
		try 
		{
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				PatientProfile patientProfile = new PatientProfile();
				patientProfile.setPatientProfileId(rs.getLong("PatientProfileId"));
				patientProfile.setPracticeProfileId(rs.getLong("PracticeProfileId"));
				patientProfile.setEmailAddress(rs.getString("EmailAddress"));
				patientProfile.setFirstName(rs.getString("FirstName"));
				patientProfile.setLastName(rs.getString("LastName"));
				patientProfile.setPreferredName(rs.getString("PreferredName"));
				patientProfile.setPrimaryPhone(rs.getString("PrimaryPhone"));
				patientProfile.setAppUserId(rs.getString("AppUserId"));
				patientProfile.setRegistrationDate(rs.getString("RegistrationDate"));
				patientProfile.setDateOfBirth(rs.getString("DateOfBirth"));
				profileList.add(patientProfile);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			databaseConnection.closeConnection(connection);
		}*/
	
		return profileList;
	}
	
}
