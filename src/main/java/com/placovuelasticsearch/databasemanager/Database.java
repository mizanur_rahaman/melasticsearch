package com.placovuelasticsearch.databasemanager;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource(value = "classpath:database.properties")
public class Database {
	
	@Value("${database.driver}") 
	private String driver;
	
	@Value("${database.url}") 
	private String url;
	
	@Value("${database.username}") 
	private String username;
	
	@Value("${database.password}") 
	private String password;
	
	 public String getDriver() {
			return driver;
		}
	public void setUDriver(String driver) {
		this.driver = driver;
	}
      public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Bean
    public static PropertySourcesPlaceholderConfigurer
      propertySourcesPlaceholderConfigurer() {
       return new PropertySourcesPlaceholderConfigurer();
    }
}
