package com.placovuelasticsearch.databasemanager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DatabaseConnection {
	
	@Autowired Database database;
	
	public Connection getConnection()
	{
		Connection connection = null;
	    try{
	    	Class.forName(database.getDriver());
	    	connection = DriverManager.getConnection(database.getUrl(),database.getUsername(), database.getPassword());
	    	System.out.println("database connection: "+connection);
	    }
	    catch (Exception e){
	      e.printStackTrace();
	    }
	    return connection;
	}
	public void closeConnection(Connection connection)
	{
		try{
			connection.close();
		}
		catch(SQLException ex){
			ex.printStackTrace();
		}
	}
	
}
