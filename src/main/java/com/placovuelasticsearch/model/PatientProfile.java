package com.placovuelasticsearch.model;

public class PatientProfile {
	  private long patientProfileId;
	  private long practiceProfileId;
	  private String firstName;
	  private String lastName;
	  private String preferredName;
	  private String primaryPhone;
	  private String otherPhone;
	  private String emailAddress;
	  private String appUserId;
	  private String registrationDate;
	  private String dateOfBirth;
	public long getPatientProfileId() {
		return patientProfileId;
	}
	public void setPatientProfileId(long patientProfileId) {
		this.patientProfileId = patientProfileId;
	}
	public long getPracticeProfileId() {
		return practiceProfileId;
	}
	public void setPracticeProfileId(long practiceProfileId) {
		this.practiceProfileId = practiceProfileId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPreferredName() {
		return preferredName;
	}
	public void setPreferredName(String preferredName) {
		this.preferredName = preferredName;
	}
	public String getPrimaryPhone() {
		return primaryPhone;
	}
	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}
	public String getOtherPhone() {
		return otherPhone;
	}
	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getAppUserId() {
		return appUserId;
	}
	public void setAppUserId(String appUserId) {
		this.appUserId = appUserId;
	}
	public String getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
}
