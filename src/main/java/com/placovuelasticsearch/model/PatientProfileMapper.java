package com.placovuelasticsearch.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
public class PatientProfileMapper implements RowMapper<PatientProfile> {
	
    public PatientProfile mapRow(ResultSet rs, int rowNumber) throws SQLException {
    	PatientProfile patientProfile = new PatientProfile();
        patientProfile.setPatientProfileId(rs.getLong("PatientProfileId"));
		patientProfile.setPracticeProfileId(rs.getLong("PracticeProfileId"));
		patientProfile.setEmailAddress(rs.getString("EmailAddress"));
		patientProfile.setFirstName(rs.getString("FirstName"));
		patientProfile.setLastName(rs.getString("LastName"));
		patientProfile.setPreferredName(rs.getString("PreferredName"));
		patientProfile.setPrimaryPhone(rs.getString("PrimaryPhone"));
		patientProfile.setAppUserId(rs.getString("AppUserId"));
		patientProfile.setRegistrationDate(rs.getString("RegistrationDate"));
		patientProfile.setDateOfBirth(rs.getString("DateOfBirth"));
        return patientProfile;
    }
}