package com.placovuelasticsearch.elasticsearchmanager;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ElasticSearchManager {

	@Autowired
	ElasticSearch elasticSearch;
	
	private static Client client = null;
	
	public void createClient() {
		
		if(client != null)
		{
			System.out.println("elastic search client already created: " + client);
			return;
		}
		
		Settings settings = Settings.builder().put("cluster.name", elasticSearch.getClusterName()).build();
		try {
			int port = Integer.parseInt(elasticSearch.getPort());
			client = new PreBuiltTransportClient(settings).addTransportAddress(
					new InetSocketTransportAddress(InetAddress.getByName(elasticSearch.getHost()), port));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		System.out.println("elastic search client newly created: " + client);

	}

	public Boolean insertDocument(String index, String type, String jsonObject, String id) {
		IndexResponse response = client.prepareIndex(index, type, id).setSource(jsonObject).get();
		return true; 
	}

	public Boolean updateDocument(String index, String type, String jsonObject, String id) {
		Boolean success = false;
		UpdateRequest updateRequest = new UpdateRequest(index, type, id).doc(jsonObject);
		try {
			client.update(updateRequest).get();
			success = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}

	public Boolean deleteIndex(String index, String type) {
		System.out.println("type: "+type);
		try {
			client.admin().indices().prepareDelete(index).execute().get();
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} 
		return true;
	}
	public Boolean deleteDocumentById(String index, String type, String id) {
		DeleteResponse response = client.prepareDelete(index, type, id).get();
		return true;
	}

	public Map<String,Object> getDocumentById(String index, String type, String id) {
		
		GetResponse getResponse = client.prepareGet(index, type, id).get();
		System.out.println(getResponse);
		Map<String, Object> document = getResponse.getSource();
		System.out.println(document);
		return document;
	}

	public SearchHit[] getDocuments(String index, String type, int startIndex, int size)
	{
		System.out.println("-----------getDocuments-----------");
		
		 SearchResponse response = client.prepareSearch(index)
                 .setTypes(type)
                 .setSearchType(SearchType.QUERY_AND_FETCH)
                 .setFrom(startIndex).setSize(size).setExplain(true)
                 .execute()
                 .actionGet();
		 
		SearchHit[] results = response.getHits().getHits();
		
		System.out.println("Total Document: " + results.length);
		

		/*for (SearchHit hit : results) 
		{
			System.out.println("------------------------------");
			Map<String,Object> result = hit.getSource();   
			System.out.println(hit.getId());
			System.out.println(result);
		}
		*/
		
		return results;
	}
	
	public SearchHit[] getDocumentsByMultiSearchQuery(String index, String type,QueryBuilder boolQuery, int startIndex, int size) {
		System.out.println("-----------getDocumentsByMultiSearchQuery-----------");
		SearchResponse response = client.prepareSearch(index).setTypes(type).setSearchType(SearchType.QUERY_AND_FETCH)
				.setQuery(boolQuery).setFrom(startIndex).setSize(size).setExplain(true).execute().actionGet();
		SearchHit[] results = response.getHits().getHits();
		System.out.println("Total Document: " + results.length);

		/*for (SearchHit hit : results) {
			System.out.println("------------------------------");
			Map<String, Object> result = hit.getSource();
			System.out.println(result);
		}*/
		return results;
	}
}
