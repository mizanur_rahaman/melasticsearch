package com.placovuelasticsearch.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.placovuelasticsearch.dao.PatientProfileDao;
import com.placovuelasticsearch.elasticsearchmanager.ElasticSearchManager;
import com.placovuelasticsearch.model.PatientProfile;
@Service
public class PatientProfileService {
	@Autowired ElasticSearchManager elasticSearchManager;
	@Autowired PatientProfileDao patientProfileDao;
	public void loadPatientProfileListToElasticNode(JdbcTemplate jdbcTemplate)
	{
		patientProfileDao.setJdbcTemplate(jdbcTemplate);
		List<PatientProfile> patientProfileList = patientProfileDao.getPatientProfileList();
		int size = patientProfileList.size();

		System.out.println(size);
		elasticSearchManager.createClient();
    	elasticSearchManager.deleteIndex("patientprofile", "patientprofile");
    	System.out.println("delete index");
		
		
		ObjectMapper mapper = new ObjectMapper();
		
		for(int i=0;i<size;i++)
		{
			PatientProfile patientProfile = patientProfileList.get(i);
			String jsonInString;
			try {
				jsonInString = mapper.writeValueAsString(patientProfile);
				elasticSearchManager.insertDocument("patientprofile", "patientprofile", jsonInString, patientProfile.getPatientProfileId()+"");
				//elasticSearchManager.insertDocument("patientprofile", "patientprofile", jsonInString, patientProfile.getPatientProfileId()+"");
				//System.out.println(jsonInString);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public JSONPObject getPatientProfileListFromElasticNode()
	{
		elasticSearchManager.createClient();
		SearchHit[] documents = elasticSearchManager.getDocuments("patientprofile", "patientprofile",0,1000);
		
		return null;
	}
	

	public ArrayList<Map<String,Object>> getPatientProfileListFromElasticNodeBySearchCrieteria(String emailAddress,String firstName,String lastName)
	{
		elasticSearchManager.createClient();
		//elasticSearchManager.removeAllDocumentOfIndex("patientprofile", "patientprofile");
		QueryBuilder boolQuery = QueryBuilders.boolQuery()

								  .must(QueryBuilders.prefixQuery("emailAddress", emailAddress))
				    			  .must(QueryBuilders.prefixQuery("firstName", firstName))
				    			  .must(QueryBuilders.prefixQuery("lastName", lastName));
	
		SearchHit[] results = elasticSearchManager.getDocumentsByMultiSearchQuery("patientprofile", "patientprofile", boolQuery, 0, 100);
		
		ObjectMapper mapper = new ObjectMapper();
		ArrayList<Map<String,Object>> profileList = new ArrayList<Map<String,Object>>();
		for (SearchHit hit : results) 
		{
			//PatientProfile patientProfile = mapper.convertValue(hit.getSource(), PatientProfile.class);
			profileList.add(hit.getSource());   
		}
		return profileList;
	}
}
