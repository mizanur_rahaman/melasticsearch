package com.placovuelasticsearch.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.placovuelasticsearch.service.PatientProfileService;
import com.placovuelasticsearch.spring.Employee;
import com.placovuelasticsearch.spring.PatientCron;
import com.placovuelasticsearch.spring.PatientDao;
import com.placovuelasticsearch.spring.PatientProfile;

import org.springframework.jdbc.core.JdbcTemplate;

@Controller
public class LoginController {
	@Autowired PatientProfileService patientProfileService;
	@Autowired JdbcTemplate jdbcTemplate;
	@Autowired  PatientDao patientDao;
	@RequestMapping("/")
	public ModelAndView login() {
		ModelAndView mv = new ModelAndView("elasticSearch");
		System.out.println("Login action");
		/*patientDao.setTemplate(jdbcTemplate);
		Employee employee = new Employee ();
		employee.setEmail("dablu@gmail.com");
		employee.setName("hablu");
        patientDao.save(employee);
        employee.setName("hasanul");
        employee.setId(1);
        patientDao.update(employee);
		employee = patientDao.getPatientById(1);
		PatientCron patientcron = new PatientCron();		
        System.out.println("Employee name is " + employee.getEmail() + " " + employee.getName());
        List<Employee> employees = patientDao.getEmployees();
        System.out.println("Total Employee: " + employees.size());
        System.out.println(employees.get(1).getEmail() + " last row " + employees.get(1).getName());
        //patientDao.delete(3);*/
		return mv;
	}
}
