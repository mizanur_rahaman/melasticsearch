package com.placovuelasticsearch.controller;

import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.placovuelasticsearch.service.PatientProfileService;
@Controller
public class PatientProfileController {
	@Autowired PatientProfileService patientProfileService;
	
	@RequestMapping(value="/getPatientProfileList",method=RequestMethod.POST)
	public @ResponseBody ArrayList<Map<String,Object>> getPatientProfileLis(HttpServletRequest request,HttpServletResponse response) 
	{
		System.out.println("----------Get Patient Profile List action ----------------");
		//patientProfileService.loadPatientProfileListToElasticNode();
		String emailAddress = request.getParameter("emailAddress") != null ? request.getParameter("emailAddress").trim().toLowerCase() : "";
		String firstName = request.getParameter("firstName") != null ? request.getParameter("firstName").trim().toLowerCase() : "";
		String lastName = request.getParameter("lastName") != null ? request.getParameter("lastName").trim().toLowerCase() : "";
		
		ArrayList<Map<String,Object>> patientProfileList = patientProfileService.getPatientProfileListFromElasticNodeBySearchCrieteria(emailAddress,firstName,lastName);
	
		return patientProfileList;
	}
}
