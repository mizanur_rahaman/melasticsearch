package com.placovuelasticsearch.spring;

public class PatientProfile {
	  private long PatientProfileId;
	  private long PracticeProfileId;
	  private String FirstName;
	  private String LastName;
	  private String PreferredName;
	  private String PrimaryPhone;
	  private String OtherPhone;
	  private String EmailAddress;
	  private String AppUserId;
	  private String RegistrationDate;
	  private String DateOfBirth;
	public long getPatientProfileId() {
		return PatientProfileId;
	}
	public void setPatientProfileId(long patientProfileId) {
		PatientProfileId = patientProfileId;
	}
	public long getPracticeProfileId() {
		return PracticeProfileId;
	}
	public void setPracticeProfileId(long practiceProfileId) {
		PracticeProfileId = practiceProfileId;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getPreferredName() {
		return PreferredName;
	}
	public void setPreferredName(String preferredName) {
		PreferredName = preferredName;
	}
	public String getPrimaryPhone() {
		return PrimaryPhone;
	}
	public void setPrimaryPhone(String primaryPhone) {
		PrimaryPhone = primaryPhone;
	}
	public String getOtherPhone() {
		return OtherPhone;
	}
	public void setOtherPhone(String otherPhone) {
		OtherPhone = otherPhone;
	}
	public String getEmailAddress() {
		return EmailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}
	public String getAppUserId() {
		return AppUserId;
	}
	public void setAppUserId(String appUserId) {
		AppUserId = appUserId;
	}
	public String getRegistrationDate() {
		return RegistrationDate;
	}
	public void setRegistrationDate(String registrationDate) {
		RegistrationDate = registrationDate;
	}
	public String getDateOfBirth() {
		return DateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		DateOfBirth = dateOfBirth;
	}

	
}
