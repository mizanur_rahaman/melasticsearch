package com.placovuelasticsearch.spring;
 
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.placovuelasticsearch.databasemanager.Database;
 
@Configuration
public class SpringJDBCConfiguration {
   
	@Autowired JdbcTemplate jdbcTemplate;
	/*@Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        //MySQL database we are using
        dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        dataSource.setUrl("jdbc:sqlserver://localhost:1433;DatabaseName=PlacovuOntrackHealth");//change url
        dataSource.setUsername("sa");//change userid
        dataSource.setPassword("12345");//change pwd

        return dataSource;
    }
 
    @Bean
    public JdbcTemplate jdbcTemplate() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource());
        return jdbcTemplate;
    }*/
 
    public PatientDao patientDao(){
    	PatientDao patientDao = new PatientDao();
    	patientDao.setTemplate(jdbcTemplate);
        return patientDao;
    }
 
}