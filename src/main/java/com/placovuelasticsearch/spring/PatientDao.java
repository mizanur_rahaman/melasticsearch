package com.placovuelasticsearch.spring;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
@Repository
public class PatientDao {
 
    private JdbcTemplate jdbcTemplate;
    
    public void setTemplate(JdbcTemplate template) {  
        this.jdbcTemplate = template;  
    } 
 
    public List<Employee> getEmployees()
    {
    	String sql = "select *from Employee";
    	List<Employee> employees = jdbcTemplate.query(sql, 
                        new EmployeeMapper());
    	return employees;
    }
    public int save(Employee p){  
    	String sql="insert into Employee(name,email)values('"+p.getName()+"','"+p.getEmail()+"')"; 
        return jdbcTemplate.update(sql);  
    }  
    public int update(Employee p){  
        String sql="update Employee set name='"+p.getName()+"' where id="+p.getId();  
        return jdbcTemplate.update(sql);  
    }  
    public int delete(int id){  
        String sql="delete from Employee  where id="+id+"";  
        return jdbcTemplate.update(sql);  
    }  
    public Employee getPatientById(int id){  
        String sql="select * from Employee  where id=?";  
        return jdbcTemplate.queryForObject(sql, new Object[]{id},new EmployeeMapper());  
    } 
 
}