<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Placovu Elastic Search</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript">
	function getPatientProfileList()
	{
		var dataArray = {
				"emailAddress" : $("#emailAddress").val(),
				"firstName" : $("#firstName").val(),
				"lastName" : $("#lastName").val()
		}
		 $.ajax({
			 url: "<%=request.getContextPath().toString()%>/getPatientProfileList",
			 method: "post",
			 data: dataArray,
		 	 dataType : "json",
		 	 global : false,
			 success: function(patientProfileList){
				 $("#patientProfileList").html("");
				 var html = "";
                 var size = patientProfileList.length;
                 for(var i=0;i<size;i++)
                 {
                	 html+="<tr>"
     			           +"  <td>"+patientProfileList[i].emailAddress+"</td>"
     			           +"  <td>"+patientProfileList[i].firstName+"</td>"
     			           +"  <td>"+patientProfileList[i].lastName+"</td>"
     			           +"  <td>"+patientProfileList[i].primaryPhone+"</td>"
     			           +"  <td>"+patientProfileList[i].dateOfBirth+"</td>"
    			      	   +"</tr>";	
                 }	 
                 $("#patientProfileList").html(html);
		 	 },
		 	 error: function(result){
		 	 }
		});
	}
	
	$(document).ready(function(){
		getPatientProfileList();
	});

</script>
</head>
<body>
	<div class="container">
		 <div class="panel panel-default">
	     	<div class="panel-heading"><h2>Elastic Search Demo</h2></div>
	    	  <div class="panel-body panel-primary">
			    	<form class="form-inline">
					  <div class="form-group">
					    <label for="email">Email address:</label>
					    <input type="text" class="form-control" id="emailAddress" onkeyup="getPatientProfileList()">
					  </div>
					  <div class="form-group">
					    <label for="pwd">First Name:</label>
					    <input type="text" class="form-control" id="firstName" onkeyup="getPatientProfileList()">
					  </div>
					  <div class="form-group">
					    <label for="pwd">Last Name:</label>
					    <input type="text" class="form-control" id="lastName" onkeyup="getPatientProfileList()">
					  </div>
					</form>
				</div>
				<div class="panel-body panel-primary">
		    	 <table class="table table-striped">
				    <thead>
				      <tr>
				        <th>Email Address</th>
				        <th>Firstname</th>
				        <th>Lastname</th>
				        <th>Primary Phone</th>
				        <th>Date of Birth</th>
				      </tr>
				    </thead>
				    <tbody id="patientProfileList">
				    </tbody>
				  </table>
				 </div>
	  	</div>
	</div>
</body>
</html>